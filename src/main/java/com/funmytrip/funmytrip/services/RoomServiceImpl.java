package com.funmytrip.funmytrip.services;

import com.funmytrip.funmytrip.customexception.InvalidBookingException;
import com.funmytrip.funmytrip.models.Booking;
import com.funmytrip.funmytrip.models.Room;
import com.funmytrip.funmytrip.models.User;
import com.funmytrip.funmytrip.repositories.BookingRepository;
import com.funmytrip.funmytrip.repositories.RoomRepository;
import com.funmytrip.funmytrip.searchcriterias.RoomSearchCriteria;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {

    private final RoomRepository roomRepository;
    private final BookingRepository bookingRepository;

    public RoomServiceImpl(RoomRepository roomRepository, BookingRepository bookingRepository) {
        this.roomRepository = roomRepository;
        this.bookingRepository = bookingRepository;
    }

    @Override
    public Room getRoomById(Long id) {
        return roomRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Room not found with id " + id));
    }

    @Override
    public List<Room> getAllRooms() {
        return roomRepository.findAll();
    }

    @Override
    public List<Room> searchRooms(RoomSearchCriteria criteria) {
        if (criteria.isEmpty()) {
            throw new IllegalArgumentException("At least one search criteria must be provided.");
        }

        if (criteria.hasCheckIn() && criteria.hasCheckOut()) {
            return roomRepository.findAvailableRooms(criteria.getCheckIn(), criteria.getCheckOut());
        }

        throw new IllegalArgumentException("Invalid search criteria provided.");
    }

    @Override
    public void bookRoom(User user, Room room, LocalDate checkinDate, LocalDate checkoutDate) throws InvalidBookingException {
        List<Booking> bookings = room.getBookings();
        for (Booking booking : bookings) {
            if (booking.getCheckinDate().isBefore(checkoutDate) && checkinDate.isBefore(booking.getCheckoutDate())) {
                throw new InvalidBookingException("Room is already booked for the given date range");
            }
        }
        Booking booking = new Booking(user, room, checkinDate, checkoutDate);
        room.addBooking(booking);
        roomRepository.save(room);
    }

    @Override
    public Room createRoom(Room room) {
        roomRepository.save(room);
        return room;
    }

    @Override
    public Room updateRoom(Room room) {
        return roomRepository.save(room);
    }

    @Override
    public void deleteRoom(Long id) {
        roomRepository.deleteById(id);
    }
}
