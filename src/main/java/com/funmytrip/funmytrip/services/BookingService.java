package com.funmytrip.funmytrip.services;

import com.funmytrip.funmytrip.customexception.InvalidBookingException;
import com.funmytrip.funmytrip.models.Booking;

import java.time.LocalDate;
import java.util.List;

public interface BookingService {
    Booking createBooking(Booking booking);

    Booking getBookingById(Long id);

    List<Booking> getBookingsByUser(Long userId);

    boolean cancelBooking(Long bookingId) throws InvalidBookingException;
    Booking updateBooking(Long bookingId, Booking booking) throws InvalidBookingException;

    List<Booking> getBookingsByUserAndDates(Long userId, LocalDate checkinDate, LocalDate checkoutDate);
}