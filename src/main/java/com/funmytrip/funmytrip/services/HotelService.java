package com.funmytrip.funmytrip.services;

import com.funmytrip.funmytrip.dtos.HotelDTO;
import com.funmytrip.funmytrip.models.Hotel;

import java.time.LocalDate;
import java.util.List;

public interface HotelService {
    Hotel getHotelById(Long id);

    List<Hotel> getAllHotels();

    Hotel createHotel(Hotel hotel);

    Hotel updateHotel(Long id, Hotel hotel);

    void deleteHotel(Long id);

    List<HotelDTO> searchHotels(String destination, LocalDate checkinDate, LocalDate checkoutDate, int rooms, int adults, int children);
}

