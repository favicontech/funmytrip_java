package com.funmytrip.funmytrip.services;

import com.funmytrip.funmytrip.customexception.ResourceNotFoundException;
import com.funmytrip.funmytrip.dtos.HotelDTO;
import com.funmytrip.funmytrip.models.Hotel;
import com.funmytrip.funmytrip.repositories.HotelRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HotelServiceImpl implements HotelService {
    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private ModelMapper modelMapper;

    public Hotel getHotelById(Long hotelId) {
        return hotelRepository.findById(hotelId)
                .orElseThrow(() -> new ResourceNotFoundException("Hotel", "Id", hotelId));
    }

    public List<Hotel> getAllHotels() {
        return hotelRepository.findAll();
    }

    public Hotel createHotel(Hotel hotel) {
        return hotelRepository.save(hotel);
    }

    public Hotel updateHotel(Long hotelId, Hotel hotel) {
        Hotel existingHotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new ResourceNotFoundException("Hotel", "id", hotelId));

        existingHotel.setName(hotel.getName());
        existingHotel.setAddress(hotel.getAddress());
        existingHotel.setDescription(hotel.getDescription());

        return hotelRepository.save(existingHotel);
    }

    public void deleteHotel(Long hotelId) {
        Hotel hotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new ResourceNotFoundException("Hotel", "id", hotelId));
        hotelRepository.delete(hotel);
    }

    @Override
    public List<HotelDTO> searchHotels(String destination, LocalDate checkinDate, LocalDate checkoutDate, int rooms, int adults, int children) {
        List<HotelDTO> hotels;

        // If all parameters are null, return all hotels
        if (destination == null && (checkinDate == null || checkoutDate == null || rooms <=0 || adults <=0 || children <0)) {
            List<Hotel> allHotels = hotelRepository.findAll();
            hotels = allHotels.stream()
                    .map(hotel -> modelMapper.map(hotel, HotelDTO.class))
                    .collect(Collectors.toList());
        } else {
            // Otherwise, apply filters based on non-null parameters
            hotels = hotelRepository.searchHotels(destination, checkinDate, checkoutDate, rooms, adults, children);
        }

        return hotels;
    }
}
