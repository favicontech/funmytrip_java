package com.funmytrip.funmytrip.services;

import com.funmytrip.funmytrip.customexception.InvalidBookingException;
import com.funmytrip.funmytrip.models.Booking;
import com.funmytrip.funmytrip.models.Room;
import com.funmytrip.funmytrip.models.User;
import com.funmytrip.funmytrip.repositories.BookingRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class BookingServiceImpl implements BookingService {
    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private RoomService roomService;

    @Override
    public Booking createBooking(Booking booking) {
        User user = userService.getUserById(booking.getUser().getId());
        Room room = roomService.getRoomById(booking.getRoom().getId());

        if (booking.getCheckinDate().isAfter(booking.getCheckoutDate())) {
            throw new InvalidBookingException("Checkin date cannot be after checkout date");
        }

        int numNights = (int) ChronoUnit.DAYS.between(booking.getCheckinDate(), booking.getCheckoutDate());

        if (numNights <= 0) {
            throw new InvalidBookingException("Booking must be for at least one night");
        }

        BigDecimal totalPrice = room.getPricePerNight().multiply(BigDecimal.valueOf(numNights))
                .setScale(2, RoundingMode.HALF_UP);

        booking.setUser(user);
        booking.setHotel(room.getHotel());
        booking.setNumGuests(room.getMaxGuests());
        booking.setTotalPrice(totalPrice);

        return bookingRepository.save(booking);
    }

    @Override
    public boolean cancelBooking(Long bookingId) throws InvalidBookingException {
        Booking booking = getBookingById(bookingId);
        if (booking.getCheckinDate().isBefore(LocalDate.now())) {
            throw new InvalidBookingException("Cannot cancel booking because the check-in date has already passed");
        }
        bookingRepository.delete(booking);
        return true;
    }

    @Override
    public Booking updateBooking(Long bookingId, Booking booking) throws InvalidBookingException {
        Booking existingBooking = getBookingById(bookingId);
        if (!existingBooking.getUser().equals(booking.getUser())) {
            throw new InvalidBookingException("User cannot be changed");
        }
        if (existingBooking.getCheckinDate().isBefore(LocalDate.now())) {
            throw new InvalidBookingException("Cannot update booking because the check-in date has already passed");
        }
        existingBooking.setCheckinDate(booking.getCheckinDate());
        existingBooking.setCheckoutDate(booking.getCheckoutDate());
        existingBooking.setTotalPrice(booking.getTotalPrice());
        return bookingRepository.save(existingBooking);
    }

    @Override
    public Booking getBookingById(Long id) {
        return bookingRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Booking not found with id: " + id));
    }

    @Override
    public List<Booking> getBookingsByUserAndDates(Long userId, LocalDate checkinDate, LocalDate checkoutDate) {
        User user = userService.getUserById(userId);
        return bookingRepository.findByUserAndCheckinDateBetweenAndCheckoutDateBetween(user, checkinDate,
                checkoutDate);
    }

    @Override
    public List<Booking> getBookingsByUser(Long userId) {
        return bookingRepository.findByUser(userId);
    }
}
