package com.funmytrip.funmytrip.services;

import com.funmytrip.funmytrip.customexception.InvalidBookingException;
import com.funmytrip.funmytrip.models.Room;
import com.funmytrip.funmytrip.models.User;
import com.funmytrip.funmytrip.searchcriterias.RoomSearchCriteria;

import java.time.LocalDate;
import java.util.List;

public interface RoomService {

    Room getRoomById(Long id);

    List<Room> getAllRooms();

    List<Room> searchRooms(RoomSearchCriteria criteria);

    public void bookRoom(User user, Room room, LocalDate checkinDate, LocalDate checkoutDate) throws InvalidBookingException;

    Room createRoom(Room room);

    Room updateRoom(Room room);

    void deleteRoom(Long id);
}
