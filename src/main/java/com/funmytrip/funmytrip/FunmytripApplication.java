package com.funmytrip.funmytrip;

import com.funmytrip.funmytrip.configbeans.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(AppConfig.class)
public class FunmytripApplication {

    public static void main(String[] args) {
        SpringApplication.run(FunmytripApplication.class, args);
    }

}
