package com.funmytrip.funmytrip.dtos;

import com.funmytrip.funmytrip.models.Hotel;

public class HotelDTO {
    private String name;

    private String description;

    private String address;

    private String city;

    private String state;

    private String country;

    private String zip;

    public HotelDTO() {}
    public HotelDTO(Hotel hotel) {
        this.name = hotel.getName();
        this.description = hotel.getDescription();
        this.address = hotel.getAddress();
        this.city = hotel.getCity();
        this.state = hotel.getState();
        this.country = hotel.getCountry();
        this.zip = hotel.getZip();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
