package com.funmytrip.funmytrip.controllers;

import com.funmytrip.funmytrip.models.Room;
import com.funmytrip.funmytrip.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rooms")
public class RoomController {
    @Autowired
    private RoomService roomService;

    @GetMapping("/{id}")
    public ResponseEntity<Room> getRoomById(@PathVariable(value = "id") Long roomId) {
        Room room = roomService.getRoomById(roomId);
        return ResponseEntity.ok().body(room);
    }

    @GetMapping("/hotel/{hotelId}")
    /*public ResponseEntity<List<Room>> getRoomsByHotel(@PathVariable(value = "hotelId") Long hotelId) {
        List<Room> rooms = roomService.getRoomsByHotel(hotelId);
        return ResponseEntity.ok().body(rooms);
    }*/

    @PostMapping
    public ResponseEntity<Room> createRoom(@RequestBody Room room) {
        Room newRoom = roomService.createRoom(room);
        return ResponseEntity.status(HttpStatus.CREATED).body(newRoom);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Room> updateRoom(@PathVariable(value = "id") Long roomId, @RequestBody Room room) {
        Room updatedRoom = roomService.updateRoom(room);
        return ResponseEntity.ok().body(updatedRoom);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteRoom(@PathVariable(value = "id") Long roomId) {
        roomService.deleteRoom(roomId);
        return ResponseEntity.ok().build();
    }
}

