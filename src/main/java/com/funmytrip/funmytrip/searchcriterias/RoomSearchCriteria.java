package com.funmytrip.funmytrip.searchcriterias;

import java.time.LocalDate;

public class RoomSearchCriteria {

    private String city;
    private LocalDate checkIn;
    private LocalDate checkOut;

    public RoomSearchCriteria() {}

    public RoomSearchCriteria(String city, LocalDate checkIn, LocalDate checkOut) {
        this.city = city;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
    }

    public boolean isEmpty() {
        return city == null && checkIn == null && checkOut == null;
    }

    public boolean hasCity() {
        return city != null;
    }

    public boolean hasCheckIn() {
        return checkIn != null;
    }

    public boolean hasCheckOut() {
        return checkOut != null;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;
    }
}

