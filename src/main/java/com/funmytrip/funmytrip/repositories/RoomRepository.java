package com.funmytrip.funmytrip.repositories;

import com.funmytrip.funmytrip.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
    List<Room> findByHotelId(Long hotelId);

    @Query("SELECT r FROM Room r WHERE r.id NOT IN "
            + "(SELECT b.room.id FROM Booking b WHERE "
            + "(b.checkinDate BETWEEN :checkIn AND :checkOut) OR "
            + "(b.checkoutDate BETWEEN :checkIn AND :checkOut) OR "
            + "(:checkIn BETWEEN b.checkinDate AND b.checkoutDate) OR "
            + "(:checkOut BETWEEN b.checkinDate AND b.checkoutDate))")
    List<Room> findAvailableRooms(@Param("checkIn") LocalDate checkIn, @Param("checkOut") LocalDate checkOut);
}

