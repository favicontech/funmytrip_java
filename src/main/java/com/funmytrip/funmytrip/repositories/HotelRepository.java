package com.funmytrip.funmytrip.repositories;

import com.funmytrip.funmytrip.dtos.HotelDTO;
import com.funmytrip.funmytrip.models.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long> {
    @Query("SELECT new com.funmytrip.funmytrip.dtos.HotelDTO(h) FROM Hotel h LEFT JOIN Booking b ON h.id = b.hotel.id " +
            "LEFT JOIN Room r ON h.id = r.hotel.id " +
            "WHERE (h.name LIKE %:searchTerm% OR h.address LIKE %:searchTerm% " +
            "OR h.city LIKE %:searchTerm% OR h.state LIKE %:searchTerm% " +
            "OR h.country LIKE %:searchTerm%) " +
            "AND (b.id IS NULL OR (:checkinDate > b.checkoutDate OR :checkoutDate < b.checkinDate)) " +
            "AND (r.maxGuests >= :adults+:children AND h.availableRooms >= :rooms)")
    List<HotelDTO> searchHotels(@Param("searchTerm") String searchTerm,
                                @Param("checkinDate") LocalDate checkinDate,
                                @Param("checkoutDate") LocalDate checkoutDate,
                                @Param("rooms") int rooms,
                                @Param("adults") int adults,
                                @Param("children") int children);
}

