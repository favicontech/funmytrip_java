package com.funmytrip.funmytrip.repositories;

import com.funmytrip.funmytrip.models.Booking;
import com.funmytrip.funmytrip.models.Room;
import com.funmytrip.funmytrip.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Long> {

    List<Booking> findByUser(Long userId);

    List<Booking> findByRoom(Room room);

    @Query("SELECT b FROM Booking b WHERE b.user = :user AND "
            + "((b.checkinDate BETWEEN :startDate AND :endDate) OR "
            + "(b.checkoutDate BETWEEN :startDate AND :endDate))")
    List<Booking> findByUserAndCheckinDateBetweenAndCheckoutDateBetween(
            @Param("user") User user, @Param("startDate") LocalDate startDate,
            @Param("endDate") LocalDate endDate);

    @Query("SELECT b FROM Booking b WHERE b.room = :room AND "
            + "((b.checkinDate BETWEEN :startDate AND :endDate) OR "
            + "(b.checkoutDate BETWEEN :startDate AND :endDate))")
    List<Booking> findByRoomAndCheckinDateBetweenAndCheckoutDateBetween(
            @Param("room") Room room, @Param("startDate") LocalDate startDate,
            @Param("endDate") LocalDate endDate);
}