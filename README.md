# funmytrip_java



## Getting started


##Project Name##: Spring Boot Hotel Search Application: FunMyTrip

## Estimated reading time
40 minutes

## Story Outline
Collect and aggregate and index hotel data: Using this application we will collect hotel information from multiple sources and aggregate the data into a centralized database. After that we will compare based on the different-2 attributes like price etc. We can also use APIs provided by hotel providers or use web scraping techniques to extract information from their websites.

## Story Branches
BackEnd branch: https://gitlab.com/favicontech/funmytrip_java/-/tree/main(main is master branch
and development is dev branch)

FrontEnd Branch: https://gitlab.com/favicontech/funmytrip_java/-/tree/master?ref_type=heads(master is the main branch for fronend)

## Tags
Clean code and use best practices
